%\documentclass[11pt]{article}
%\usepackage[margin=2cm]{geometry}
\input{preamble}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algcompatible}
\usepackage{stmaryrd}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[colorlinks,bookmarksopen,bookmarksnumbered,citecolor=red,urlcolor=red]{hyperref}
%\usepackage[usenames]{color}
\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\definecolor{DarkRed}{rgb}{0.55,0.00,0.00}
\definecolor{DarkGreen}{rgb}{0.00,0.55,0.00}
\definecolor{Gray}{rgb}{0.95,0.95,0.95}
\definecolor{Purple}{rgb}{0.5,0.0,0.5}
\definecolor{Bittersweet}{rgb}{1.0,0.44,0.37}
\lstset{%
  language=python,
  backgroundcolor=\color{Gray},
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{Purple},
  commentstyle=\itshape\color{DarkGreen},
  stringstyle=\color{DarkRed},
  emphstyle={[2]\color{DarkBlue}},
  emphstyle=\color{Bittersweet},
  showspaces=false,
  showtabs=false,
  columns=fixed,
  framesep=2ex,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  breakatwhitespace=true,
  showstringspaces=false,
  escapeinside={(*@}{@*)},
  xrightmargin=2ex,
  xleftmargin=2ex,
}
\begin{document}
\begin{center}
\Huge{Knapsack problems, algorithms and programming in Python}
\end{center}
\begin{center}
{\huge \textsf{--- Lecture 1 ---}}
\end{center}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Imagine that you have a backpack which can carry no more than 15kg. You also have a set of items with the following weights and values (see Figure \ref{fig:knapsack}):
\begin{description}
  \setlength{\itemsep}{-0.5ex}
\item[item 0]: 1kg / \$1
\item[item 1]: 1kg / \$2
\item[item 2]: 2kg / \$2
\item[item 3]: 4kg / \$10
\item[item 4]: 12kg / \$4
\end{description}
Which items should you put into your bag to maximise its value, while not exceeding the weight limit of 15kg, and what is the maximal value in this case? This is an example of a so-called \textbf{knapsack problem}. Apart from the obvious application in bank robberies, combinatorical problems of this type arise in the practically relevant area of resource optimisation in economics, management and logistics. It turns out that working out the optimal content of the backpack can not be done in one step, for example by solving a suitable equation. Instead, we need to define the optimal value of the knapsack of a certain weight in terms of the optimal value for a smaller weight. This approach is also known as a \textit{recursive} definition. Solving a given problem by reducing it to a simpler problem is a very common trick in mathematics. As we will see below, the problem can then be solved by writing down a step-by-step procedure, which can be expressed in an \textbf{algorithm} and then implemented on a computer. In this lecture we will use the Python programming language for this.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.4\linewidth]{figures/knapsack.png}
    \caption{Knapsack problem (source: wikipedia)}
    \label{fig:knapsack}
  \end{center}
\end{figure}
\noindent
Similar optimisation algorithms are used in other applications, for example in internet searches or to find the shortest route between two locations in GPS systems. In fact, most real-life problems are so complex that they can not be solved exactly. Algorithms lie at the heart of machine learning and data science, where often very large and complex problems have to be solved. It is important to find algorithms which can be run in the shortest possible time to obtain a solution a quickly as possible.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The general problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let us first write down the problem mathematically in a general way.
Assume that we have $n$ items with integer\footnote{The set of positive numbers is denoted by $\mathbb{N}=1,2,3,\dots$ and we write $\mathbb{N}_0=0,1,2,3,\dots$ for the set of non-negative numbers.} weights $w_0,w_1,\dots,w_{n-1}\in \mathbb{N}$ and positive values $v_0,v_1,\dots,v_{n-1}>0$ (i.e. the $k$-th item has weight $w_k$ and value $v_k$. We label the items starting with zero, since this is the convention used by the Python programming language, which we will use later). In the example at the beginning of this document we would have $n=5$ items with weights $w_0=1$, $w_1=1$, $w_2=2$, $w_3=4$, $w_4=12$ and values $v_0=1$, $v_1=2$, $v_2=2$, $v_3=10$, $v_4=4$.

Given a maximal weight $W\in\mathbb{N}$ ($W=15$ in our example), we now want to find $n$ integer numbers $x_0,\dots,x_{n-1}$ such that the sum
\begin{equation}
  \sum_{k=0}^{n-1} v_kx_k\label{eqn:knapsack_1}
\end{equation}
is maximised subject to the constraint
\begin{equation}
  \sum_{k=0}^{n-1} w_kx_k \le W.\label{eqn:knapsack_2}
\end{equation}
Here $x_k$ is the number of times we pack item $k$. The inequality in Eq. \eqref{eqn:knapsack_2} says that the weight of the knapsack should not exceed the maximal weight $W$. The maximal value $M$ of the sum in Eq. \eqref{eqn:knapsack_1} subject to the constraint in Eq. \eqref{eqn:knapsack_2} can be expressed in compact form 
\begin{equation}
  M = \max_{x_0,\dots,x_{n-1}}\left(\sum_{k=0}^{n-1} v_kx_k\right)
  \qquad\text{subject to\qquad $\sum_{k=0}^{n-1}w_kx_k\le W$ and with $x_k\ge 0$}.\label{eqn:mW_definition}
\end{equation}
Here writing ``$\displaystyle{\max_{x_0,x_1,\dots,x_{n-1}}}$'' means that we are looking for the maximum over all possible choices of the numbers $x_0,\dots,x_{n-1}$.
We consider two cases:
\begin{itemize}
\item If each item can only be used once, i.e. $x_k\in\{0,1\}$, this is the \textbf{0/1 knapsack problem}.
\item If each item can be used an arbitrary number of times, i.e. $x_k\in\mathbb{N}_0$, this is the \textbf{unbounded knapsack problem}.
\end{itemize}
Clearly, the example at the very beginning of this section is a 0/1 knapsack problem. The solution is $x_0=x_1=x_2=x_3=1$, $x_4=0$.

Since it turns out that unbounded knapsack problems are a bit easier, we will only consider those here. The solution of the 0/1 knapsack problem uses very similar techniques, and is written down in the appendix, as an additional challenge.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Unbounded knapsack problem}\label{sec:knapsack_unbounded}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Step-by-step solution}\label{sec:knapsack_unbounded}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To solve the unbounded knapsack problem, let us generalise Eq. \eqref{eqn:mW_definition} and construct the following function $m[\omega]$ for \textit{all} integer values $\omega=0,1,\dots,W$:
\begin{equation}
  m[\omega] = \max_{x_0,\dots,x_{n-1}}\left(\sum_{k=0}^{n-1}v_kx_k\right)\qquad\text{subject to\qquad $\sum_{k=0}^{n-1}w_kx_k\le \omega$ and with $x_k\ge 0$}.
  \label{eqn:unbounded_function}
\end{equation}
This function is the maximal value of a knapsack with weight not exceeding $\omega$, and in particular $m[W]=M$ is the number we want to know. It looks like we have made the problem more complicated, since instead of only computing $M=m[W]$, we now also want to know $m[0]$, $m[1]$, \dots, $m[W-1]$. However, it turns out that this allows us to compute $m[W]$ step-by-step, by considering the intermediate values. The function $m[\omega]$ defined in Eq. \eqref{eqn:unbounded_function} has the following two properties:
\begin{description}
\item[Property 1:] $m[0] = 0$
\item[Property 2:] $m[\omega]=\displaystyle{\begin{cases}
  0 & \text{if $w_j>\omega$ for all $j=0,1,\dots,n-1$}\\ 
  \displaystyle{\max_{0\le i<n,w_i\le\omega}\{m[\omega-w_i]+v_i\}} & \text{otherwise}
  \end{cases}}
  $
\end{description}
The first property follows from the fact that (since all weights are positive), for $\omega=0$ the only way of satisfying the constraint in Eq. \eqref{eqn:unbounded_function} is to set $x_0=x_1=\dots=x_{n-1}=0$. In other words, the only way of making sure the knapsack has weight zero is to leave it empty.

The second property is a bit more difficult to understand, but the key to solving the problem. Let us first discuss the notation, in particular what we mean by ``$\displaystyle{\max_{0\le i<n,w_i\le\omega}}\{m[\omega-w_i]+v_i\}$''. This expression is a shorthand for saying that we calulate the number $m[\omega-w_i]+v_i$ for all indices $i$ between $0$ and $n-1$ for which $w_i\le \omega$, and then take the maximal value. For example, if $\omega=2$, $w_0=1$, $w_1=2$, $w_2=4$ and $v_0=2$, $v_1=3$, $v_2=9$, then
\begin{equation}
  \max_{0\le i<2,w_i\le 2} \{m[2-w_i]+v_i\} = \max\{m[2-w_0]+v_0,m[2-w_1]+v_1\},
\end{equation}
i.e. we take the maximal of the two values $m[2-w_0]+v_0$ and $m[2-w_1]+v_1$. Note that $w_2>\omega$, so we do not consider the index $i=2$ when taking the maximum.

Now let's get back to what the second property of $m[\omega]$ means. Assume, for a moment, that we know the value of the function $m$ for all values $0,1,\dots,\omega-1$. How can we pack a knapsack with weight of no more than $\omega$? Well, this knapsack can contain any of the items which have a weight not exceeding $\omega$. If it contains item $k$, then its
maximal value is at least the maximal value of the knapsack with weight $\omega-w_k$ plus the value $v_k$ of the item that we added, in other words $m[\omega-w_k]+v_k$. So what we should do to get $m[\omega]$, is to inspect all values $m[\omega-w_k]+v_k$ and pick the largest one; this is exactly what the second property states in mathematical form:
\begin{equation*}
  \max_{0\le i<n,w_i\le\omega}\{m[\omega-w_i]+v_i\}.
\end{equation*}
To be precise, we also have to say what the value of $m[\omega]$ is if all of the weights are heavier than $\omega$ (i.e. if $w_j>\omega$ for all $j=0,1,\dots,n-1$). In this case the only thing we can do is leave the knapsack empty, such that $m[\omega]=0$. Note that we have defined the function $m$ \textit{recursively}: since all weights $w_j$ are positive integers, the second property says the value $m[\omega]$ can be computed from values $m[\omega']$ with $\omega'<\omega$. This allows us to compute $m$ for any value $\omega$, since we can compute $m[\omega]$ step-by-step: once we know $m[0]$ we can compute $m[1]$. Knowing $m[0]$ and $m[1]$ we get $m[2]$ and so on. Mathematically this can also seen as an example of \textit{strong induction}: starting from a base case ($\omega=0$) we recursively construct further solutions using the induction step encoded in Property 2.

Let's try this out for a simple example. For this we again assume that we have the following weights and values: $w_0=1$, $w_1=2$, $w_2=4$ and $v_0=2$, $v_1=3$, $v_2=9$ and want to know the optimal value of the knapsack not exceeding a weight of $W=5$.
\begin{description}
\item[$\boldsymbol{\omega=0}$:] Obviously $m[0]=0$, since only the empty knapsack has a weight of 0.
\item[$\boldsymbol{\omega=1}$:] The only thing we can do in this case is put item 0 into the knapsack, the other items weigh more than 1. Its value in this case is $m[1]=m[1-w_0]+v_0=m[0]+2=0+2=2$
\item[$\boldsymbol{\omega=2}$:] Now we can do two things:
\begin{enumerate}
\item We can put only item 1 into the otherwise empty knapsack. If we do this, the total value is $m[2-w_1]+v_1=m[0]+3=0+3=3$
\item We can take the knapsack of weight not exceeding $2-w_0=1$, and add item 0 again. In this case the value is $m[2-w_0]+v_0=m[1]+2=4$.
\end{enumerate}
Clearly, the latter is better, and we therefore get
\begin{equation*}
m[2] = \max\{m[2-w_0]+v_0,m[2-w_1]+v_1\}=\max\{m[1]+2,m[0]+3\}=\max\{4,3\}=4.
\end{equation*}
\item[$\boldsymbol{\omega=3}$:] Item 2 will still not fit into the knapsack, so we have two choices again: add item 0 to the knapsack of weight not exceeding $3-w_0=2$, or add item 1 to the knapsack of weight not exceeding $3-w_1=1$. We find (see Figure \ref{fig:recursion}, top):
  \begin{figure}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{figures/recursion.pdf}
    \caption{Calculation of $m[3]$ (top) and $m[4]$ (bottom).}
    \label{fig:recursion}
  \end{center}
\end{figure}

\begin{equation*}
m[3]=\max\{m[3-w_0]+v_0,m[3-w_1]+v_1\}=\max\{m[2]+2,m[1]+3\}=\max\{4+2,2+3\}=6
\end{equation*}
\item[$\boldsymbol{\omega=4}$:] Now item 2 does fit in, so there are three choices:
\begin{enumerate}
  \item Add item 0 to the knapsack of weight not exceeding $4-w_0=3$
  \item Add item 1 to the knapsack of weight not exceeding $4-w_1=2$
  \item Add item 2 to the knapsack of weight not exceeding $4-w_2=0$
\end{enumerate}
This gives (see Figure \ref{fig:recursion}, bottom):
\begin{equation*}
  \begin{aligned}
    m[4] &= \max\{m[4-w_0]+v_0,m[4-w_1]+v_1,m[4-w_2]+v_2\}\\
    &= \max\{m[3]+2,m[2]+3,m[0]+9\}\\
    &= \max\{6+2,4+3,0+9\} = 9
  \end{aligned}
\end{equation*}
\item[$\boldsymbol{\omega=5}$:] Proceeding as above, we finally get
\begin{equation*}
  \begin{aligned}
    m[5] &= \max\{m[5-w_0]+v_0,m[5-w_1]+v_1,m[5-w_2]+v_2\}\\
    &= \max\{m[4]+2,m[3]+3,m[1]+9\}\\
    &= \max\{9+2,6+3,2+9\} = 11
  \end{aligned}
\end{equation*}
\end{description}
\begin{figure}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{figures/final_m_table.pdf}
    \caption{Final table with $m[0]$, $m[1]$, \dots, $m[5]$.}
    \label{fig:final_m_table}
  \end{center}
\end{figure}
The final table with $m[0]$, $m[1]$, \dots, $m[5]$ is shown in Figure \ref{fig:final_m_table}. So this means that the maximal value of a knapsack with weight not exceeding $W=5$ is $M=m[5]=11$. Looking back through the individual steps above, it turns out that the optimal choice is to pick one each of items 0 and 2.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{An algorithm for the unbounded knapsack problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The procedure above can be writting down in general form as an algorithm which calculates $m[W]$, given the weights and values:
\begin{algorithm}[H]
  \caption{Unbounded knapsack problem. Input: weights $w_0,w_1,\dots,w_{n-1}\in\mathbb{N}$ and values $v_0,v_1,\dots,v_{n-1}>0$, maximal weight $W\in\mathbb{N}$. Output: $M=m[W]$.}
  \label{alg:knapsack_unbounded}
  \begin{algorithmic}[1]
    \STATE{Set $m[0]\mapsfrom0$}
    \FOR{$\omega=1,\dots,W$}
    \STATE{Set $m[\omega]\mapsfrom0$}
    \FOR{$k=0,\dots,n-1$}
    \IF{$w_k\le \omega$}
    \STATE{$m[\omega]\mapsfrom\max\{m[\omega],m[\omega-w_k]+v_k\}$}
    \ENDIF
    \ENDFOR
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\noindent
Here the notation $a\mapsfrom b$ means: ``Set the value of $a$ to
$b$'', e.g. $a\mapsfrom 2$ with set $a$ to 2. Note that lines 3-8
calculate $\max\{m[\omega-w_0]+v_0,\dots,m[\omega-w_{n-1}]+v_{n-1}\}$
by inspecting each of the items in turn, looping over
$k=0,1,\dots,n-1$. Since we initialise $m[\omega]$ with zero, we get
$m[\omega]=0$ if all items are heavier than $\omega$.
\newpage
\begin{center}
\Huge{Knapsack problems, algorithms and programming in Python}
\end{center}
\begin{center}
{\huge \textsf{--- Lecture 2 ---}}
\end{center}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation in Python}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
As you will have realised, solving the knapsack problem by going through the algorithm from the previous section is straightforward but quite tedious. Although each of the steps is very simple, it is very easy to make a mistake. It is much better to use a computer for this. This will also allow us to solve much larger problems (imagine that you have 20 different weights and the upper limit is $W=1000$ - you definitely do not want to solve this problem by hand).

We will use the Python programming language to implement Algorithm \ref{alg:knapsack_unbounded}; the Python code is shown in Listing \ref{lst:knapsack_unbounded}.\\
\begin{minipage}{\linewidth}
\lstinputlisting[language={python}, label=lst:knapsack_unbounded,caption={Python code the unbounded knapsack problem}]{../src/knapsack_unbounded.py}
\end{minipage}
Note that the ``\texttt{=}'' symbol has a special meaning in Python: it assigns the value of what is written on the right hand side of the equation to the variable on the left hand side. For example, \texttt{W = 5} means \textit{``Set the value of $W$ to 5.''} or just ``$W\mapsfrom 5$''. Compare in particular the following statement to line 6 in Algorithm \ref{alg:knapsack_unbounded}:
\begin{lstlisting}[language={python}]
m[omega] = max(m[omega],m[omega-w[k]]+v[k])
\end{lstlisting}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Brief guide to the Python syntax}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Although the Python syntax is very intuitive, let us have a closer look at the code in Listing \ref{lst:knapsack_unbounded}:
\begin{itemize}
\item \texttt{w=[1,1,2,4,12]} creates a list $w$, containing the entries 1,1,2,4 and 12.
\item \texttt{len(w)} calculates the length of the list $w$, i.e. the number of items it contains (5 in this case).
\item \texttt{m=np.zeros(W+1)} creates a list with $W+1$ zeros
\item \texttt{for k in range(0,n):} executes the following statements for all $k$ in the range $0,1,\dots,n-1$
\item \texttt{if (w[k] <= omega):} executes the following statements only if $w_k\le \omega$
\item \texttt{print ('weights : ',w)} prints everything inside the round brackets.
  \item Anything after the hash symbol (\#) is a comment. Comments are ignored by the computer, but help humans to understand the code.
\end{itemize}
Note that Python is case-sensitive, i.e. the variables \texttt{W} (maximal weight of the knapsack) and \texttt{w} (list of weights) are \textit{not} the same. The first two lines of Listing \ref{lst:knapsack_unbounded} loads some libraries required by Python. The other thing that is important about Python is indentation. For each if-statement or for-loop, only the indented code is executed. For example
\begin{lstlisting}[language={python}]
for k in range(0,3):
    print ('k = ',k)
    print ('---')
\end{lstlisting}
will print 
\begin{lstlisting}[language={python}]
k = 0
---
k = 1
---
k = 2
---
\end{lstlisting}
However,
\begin{lstlisting}[language={python}]
for k in range(0,3):
    print ('k = ',k)
print ('---')
\end{lstlisting}
will print 
\begin{lstlisting}[language={python}]
k = 0
k = 1
k = 2
---
\end{lstlisting}
since the last print statement is no longer part of loop-body. You can nest indentation, as shown in Listing \ref{lst:knapsack_unbounded}.

You can install Python on your own computer or run it online on\vspace{-1ex}\begin{center}\url{https://repl.it/languages/python3}\end{center}\vspace{-1ex} For this, copy the code into the editor window, and click on the \fbox{run $\blacktriangleright$} button at the top (see Figure \ref{fig:tutpoint}).
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{figures/python3_online.png}
  \end{center}
  \caption{Running Python code\label{fig:tutpoint}}
\end{figure}
  \newpage
\appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Appendix A: Some thoughts on complexity}\label{eqn:complexity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The algorithm we developed certainly looks quite complicated. Maybe we can use a much simpler approach. For example, we could simply look at all possible combinations of the items, check whether their combined weight does not exceed the threshold $W$ and then pick the combination of the largest total value. How many combinations do we have to try? An item with weight $w_k$ can not be included more than $\lfloor W/w_k\rfloor$ times\footnote{Here $\lfloor x\rfloor$ is the largest integer number which does not exceed $x$. For example, $\lfloor 2.3\rfloor=2$, $\lfloor 3\rfloor=3$, $\lfloor7.9\rfloor=7$.}. Since we could also leave out each item, the total number of combinations for $n$ items is
\begin{equation}
  \left(\left\lfloor\frac{W}{w_0}\right\rfloor+1\right)\times
  \left(\left\lfloor\frac{W}{w_1}\right\rfloor+1\right)\times\dots
  \left(\left\lfloor\frac{W}{w_{n-1}}\right\rfloor+1\right)
  \label{eqn:Ncomb}
\end{equation}
Assuming that no item weighs more than $W$, each of the factors in Eq. \eqref{eqn:Ncomb} is at least 2. This means that the number of different combinations to consider is at least $2^n$. This is ok for small $n$, but assume that we have $n=100$. Then we would have to check more than $2^{100}\approx 1.27\cdot 10^{30}$ combinations. A modern computer can carry out around $10^{10}$ calculations per second, so even in the best possible scenario this would take around $10^{20}$ seconds, or $3\cdot 10^{12}$ (three trillion) years. This is more than the age of the university, so clearly not a good idea!

On the other hand, lines 3-8 in Algorithm \ref{alg:knapsack_unbounded} are only executed $W$ times. In lines 4-8 $n$ different items are inspected. The total number of times the if statement in lines 5-7 is executed is therefore $n\cdot W$. If we assume $W=10000$ and $n=100$ (as above), then around $100\cdot 10000=10^6$ operations have to be executed, and this takes much less than a second on a modern computer.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Appendix B: The 0/1 knapsack problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Solving the 0/1 knapsack problem is a bit harder. For this, define the function $\mu[j,\omega]$ which is the maximal value of the knapsack with weight no more than $\omega$, picking only from the first $j$ items with $j=1,\dots,n$:
\begin{equation}
  \mu[j,\omega] = \max\left(\sum_{k=0}^{j-1}v_kx_k\right)\qquad\text{subject to $\sum_{k=0}^{j-1}w_kx_k\le \omega$ and with $x_k\in\{0,1\}$}.
  \label{eqn:01_function}
\end{equation}
Then $\mu[n,W]$ is the the maximal value of the 0/1-knapsack not exceeding weight $W$.
Compare this to Eq. \eqref{eqn:unbounded_function}, the only difference is that the upper limit of the sum is $i-1$ instead of $n-1$ and the constraint on $x_k$. As above, it again it looks like we made the problem more complicated: instead of computing $\mu[n,W]$, we also calculate it for a range of intermediate values $j=0,\dots,n-1$ and $\omega=0,1,\dots,W-1$. It turns out that the function $\mu[j,\omega]$ in Eq. \eqref{eqn:01_function} can be defined recursively by setting
$\mu[0,\omega]=0$ and then using
\begin{equation}
  \mu[j+1,\omega]=\begin{cases}
  \mu[j,\omega] &\text{if $w_j > \omega$}\\
  \max\{\mu[j,\omega],\mu[j,\omega-w_j]+v_j\}&\text{if $w_j\le \omega$}
  \end{cases}
\end{equation}
for $j=0,1,\dots,n-1$. Let's try to understand this. Recall that $\mu[j+1,\omega]$ is the maximal value of the knapsack with weight not exceeding $\omega$, picking only from the first $j+1$ items. To calculate $\mu[j+1,\omega]$ let's again assume that we know $\mu[j,0]$, $\mu[j,1]$, \dots, $\mu[j,\omega]$, in other words, we know the maximal value of the knapsack when picking only from the first $j$ items. If $w_j$ exceeds $\omega$, we can not include it, since then the total weight of the knapsack would be larger than $\omega$, so the best we can get is $\mu[j,\omega]$. If $w_j\le\omega$, then we can either include it or not. In the first case, the best we can get is the maximal value of the knapsack with weight $\omega-w_j$ plus the value $v_j$ of item $j$, in other words $\mu[j,\omega-w_j]+v_j$. If we do not include item $j$, then the optimal value is again $\mu[j,\omega]$. Therefore, $\mu[j+1,\omega]$ is the maximum of $\mu[j,\omega]$ and $\mu[k,\omega-w_j]+v_j$.
\begin{algorithm}[H]
  \caption{0/1 knapsack problem. Input: weights $w_0,w_1,\dots,w_{n-1}\in\mathbb{N}$ and values $v_0,v_1,\dots,v_{n-1}>0$, maximal weight $W\in\mathbb{N}$. Output: $\mu[n,W]$.}
  \label{alg:knapsack_01}
  \begin{algorithmic}[1]
    \FOR{$\omega=0,\dots,W$}
    \STATE{Set $\mu[0,\omega]\mapsfrom0$}
    \ENDFOR
    \FOR{$j=0,\dots,n-1$}
    \FOR{$\omega=1,\dots,W$}
    \IF{$w_j > \omega$}
    \STATE{Set $\mu[j+1,\omega]\mapsfrom \mu[j,\omega]\}$}
    \ELSE
    \STATE{Set $\mu[j+1,\omega]\mapsfrom\max\{\mu[j,\omega],\mu[j,\omega-w_k]+v_j\}$}
    \ENDIF
    \ENDFOR
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\lstinputlisting[language={python}, label=lst:knapsack_01,caption={Python code the 0/1 knapsack problem}]{../src/knapsack_01.py}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Challenge exercises}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{enumerate}
\item Implement the 0/1 problem yourself in Python.
\item Consider the \textbf{bounded knapsack problem}. In this case, for each $k=0,\dots,n-1$ there are $r$ items of weight $w_k$ and value $v_k$. In other words, for given $W$ we want to maximise $m[W]$ defined in Eq. \eqref{eqn:knapsack_1} subject to \eqref{eqn:knapsack_2} but now $x_k\in\{0,1,2,\dots,r\}$. Hint: reduce the problem to a 0/1 knapsack problem.
\end{enumerate}
\end{document}
