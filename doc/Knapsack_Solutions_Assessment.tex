\input{preamble}
\usepackage{amsmath,amssymb,amsthm}
\newcounter{exercise_counter}
\setcounter{exercise_counter}{0}
\usepackage{enumitem}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algcompatible}
\usepackage{stmaryrd}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[colorlinks,bookmarksopen,bookmarksnumbered,citecolor=red,urlcolor=red]{hyperref}
%\usepackage[usenames]{color}
\lstdefinelanguage[ppmd]{python}[]{python}{%
  emph={ParticleLoop,ParticleDat,PositionDat,ScalarArray,GlobalArray,Kernel,PairLoop,Constant,State,Data,IntegratorRange}
}
\definecolor{DarkBlue}{rgb}{0.00,0.00,0.55}
\definecolor{DarkRed}{rgb}{0.55,0.00,0.00}
\definecolor{DarkGreen}{rgb}{0.00,0.55,0.00}
\definecolor{Gray}{rgb}{0.95,0.95,0.95}
\definecolor{Purple}{rgb}{0.5,0.0,0.5}
\definecolor{Bittersweet}{rgb}{1.0,0.44,0.37}
\lstset{%
  language=python,
  backgroundcolor=\color{Gray},
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{Purple},
  commentstyle=\itshape\color{DarkGreen},
  stringstyle=\color{DarkRed},
  emphstyle={[2]\color{DarkBlue}},
  emphstyle=\color{Bittersweet},
  showspaces=false,
  showtabs=false,
  columns=fixed,
  framesep=2ex,
  frame=single,
  numberstyle=\tiny,
  breaklines=true,
  breakatwhitespace=true,
  showstringspaces=false,
  escapeinside={(*@}{@*)},
  xrightmargin=2ex,
  xleftmargin=2ex,
}
\newenvironment{exercise}{
  \begin{description}
  \item{\bf Exercise \refstepcounter{exercise_counter}\arabic{exercise_counter}}}{%
  \end{description}  
}

\begin{document}
\begin{center}
\Huge{Knapsack problems --- Assessment Solutions}
\end{center}
\begin{exercise}
  Following the definition, the function $f[X]$ is given by
  \begin{equation}
    f[X] = \min_{x_0,\dots,x_{n-1}}\sum_{k=0}^{n-1} x_k\qquad\text{subject to\quad$\sum_{k=0}^{n-1}x_kd_k$ and $x_j\in\mathbb{N}_0$.}
  \end{equation}
  It can be defined recursively as
  \begin{description}
  \item[Property 1:] $f[0]=0$
  \item[Property 2:]
    $\displaystyle{%
    f[\omega]=\begin{cases}
    10000&\text{if $d_k>\omega$ for all $k=0,1,\dots,n-1$}\\
    \displaystyle{\min_{0\le i<n,d_i\le \omega}}\{f[\omega-d_i]+1\}&\text{otherwise}
    \end{cases}}
    $
  \end{description}
  The first property states that zero coins are required to make an amount of 0. To understand the second property, note that to make up the amount $X$ we could use any of the coins with denomination $d_i$ not exceeding $X$. To get the minimum number of coins, look at the minimum number of ways of making the amount $X-d_i$, and add 1 since we include the coin with denomination $d_i$.
\end{exercise}
\begin{exercise}
The algorithm for calculating $f[0]$, $f[1]$, \dots, $f[X]$ is shown in Algorithm \ref{alg:change_making}.
\begin{center}
\begin{minipage}{0.9\linewidth}
  \begin{algorithm}[H]
  \caption{Change making problem. Input: denominations $d_0,d_1,\dots,d_{n-1}\in\mathbb{N}$, total sum $X\in\mathbb{N}$. Output: $f[X]$.}
  \label{alg:change_making}
  \begin{algorithmic}[1]
    \STATE{Set $f[0]\mapsfrom0$}
    \FOR{$\omega=1,\dots,X$}
    \STATE{Set $f[\omega]\mapsfrom10000$}
    \FOR{$k=0,\dots,n-1$}
    \IF{$d_k\le \omega$}
    \STATE{$f[\omega]\mapsfrom\min\{f[\omega],f[\omega-d_k]+1\}$}
    \ENDIF
    \ENDFOR
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\end{minipage}
\end{center}
\end{exercise}
\begin{exercise}
The best we can do to get a total amount of $6p$ is to use two $3p$ coins, so therefore $f[6]=2$. To get a total amount of $10p$, use two $3p$ coins and one $4p$ coin, so $f[10]=3$.
\end{exercise}
\begin{exercise}
The Python code for solving the problem is given in Listing \ref{lst:change_making}
  \lstinputlisting[language={[ppmd]{python}}, label=lst:change_making,caption={Python code the change making problem}]{../src/solutions/solution_assessment_q4.py}
\end{exercise}
\begin{exercise}
Running the code gives $f[19]=5$ and $f[100]=25$
\end{exercise}
\begin{exercise}
  \begin{enumerate}[label=(\alph*)]
    \item Start with the largest denomination, $d_2=5p$. Keep taking away $5p$ coins, until this is no longer possible. Then move on to the next-largest denomination, $d_1=2p$, and keep taking away as many $2p$ coins as you can. Finally, make up the remaining amount with $1p$ coins.
\item For general denominations, this greedy algorithm can be written as in Algorithm \ref{alg:greedy}.
\begin{center}
\begin{minipage}{0.9\linewidth}
    \begin{algorithm}[H]
  \caption{Greedy algorithm. Input: denominations $d_0,d_1,\dots,d_{n-1}\in\mathbb{N}$, total sum $X\in\mathbb{N}$. Output: $f$, the minimal number of coins required to make up $X$.}
  \label{alg:greedy}
  \begin{algorithmic}[1]
    \STATE{Set $f\mapsfrom0$}
    \FOR{$k=0,1,\dots,n-1$}
    \WHILE{$X-d_{n-1-k}\ge 0$}
    \STATE{Set $X\mapsfrom X-d_{n-1-k}$}
    \STATE{Set $f\mapsfrom f+1$}
    \ENDWHILE
    \ENDFOR
  \end{algorithmic}
\end{algorithm}
\end{minipage}
\end{center}
    \item The Python code for implementing Algorithm \ref{alg:greedy} is shown in Listing \ref{lst:greedy}
      \lstinputlisting[language={[ppmd]{python}}, label=lst:greedy,caption={Python code the greedy algorithm}]{../src/solutions/solution_assessment_q6.py}
    \item We find $f=5$ for $X=19$ and $f=25$ for $X=100$.
    \item If the denominations are $d_0=1$, $d_1=3$ and $d_2=4$ and $X=6$, the greedy algorithm would start by taking $4p$ away from $6p$, and then make up the remaining $2p$ with two $1p$ coins. Hence it would return 3, but we know from the answer to Exercise 3 that the best way of making a total amount of $X=6p$ is to use two $3p$ coins.
\end{enumerate}
\end{exercise}
\end{document}
