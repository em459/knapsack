import numpy as np

# weights w and values v
w = [1,2]
a = [2,3]
v = [4,5]

# bound on weight
W = 4
# bound on volume
A = 5

n = len(w)
m = np.zeros((A+1,W+1))

for alpha in range(1,A+1):
    for omega in range(1,W+1):
        m[alpha,omega] = 0
        for k in range(0,n):
            if (w[k] <= omega) and (a[k] <= alpha):
                m[alpha,omega] = max(m[alpha,omega],m[alpha-a[k],omega-w[k]]+v[k])

print ('weights : ',w)
print ('values  : ',v)
print ('W = ',W)
print (m[A,W])
