import numpy as np
# load the numpy library)

# weights w and values v
w = [2,3,4,5]
v = [3,4,5,6]

# bound on weight
W = 25

n = len(w)
m = np.zeros(W+1)

for omega in range(1,W+1):
    m[omega] = 0
    for k in range(0,n):
        if (w[k] <= omega):
            m[omega] = max(m[omega],m[omega-w[k]]+v[k])

print ('weights : ',w)
print ('values  : ',v)
print ('W = ',W)
print (m[W])
