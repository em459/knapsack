import numpy as np
# load the numpy library)

# weights w and values v
w = [1,2,4]
v = [2,3,9]

# bound on weight
W = 4

n = len(w)
m = np.zeros(W+1,dtype=int)

m[0] = 0

for omega in range(1,W+1):
    m[omega] = 0
    for j in range(0,n):
        if (w[j] <= omega):
            m[omega] = max(m[omega],m[omega-w[j]]+v[j])

print (m[W])
