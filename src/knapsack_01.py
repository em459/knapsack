import numpy as np

# weights w and values v
w = [1,1,2,4,12]
v = [1,2,2,10,4]

# bound on weight
W = 15

n = len(w)
m = np.zeros((n+1,W+1))

for k in range(n):
    for omega in range(1,W+1):
        if w[k] > omega:
            m[k+1][omega] = m[k][omega]
        else:
            m[k+1][omega] = max(m[k][omega],m[k][omega-w[k]]+v[k])
        
print ('weights : ',w)
print ('values  : ',v)
print ('W = ',W)
print (m[n][W])
