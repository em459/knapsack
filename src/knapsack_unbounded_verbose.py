import numpy as np
# load the numpy library)

# weights w and values v
w = [2,3,4,5]
v = [3,4,5,6]

# bound on weight
W = 8

n = len(w)
m = np.zeros(W+1,dtype=int)

print ('weights : ',w)
print ('values  : ',v)
print ('W = ',W)
print ('')
print (r'\begin{description}')

for omega in range(1,W+1):
    s = r'\item[$\boldsymbol{\omega='+str(omega)+']:}$'+'\n'
    s += r' \begin{equation*}'+'\n'+r'  \begin{aligned}'+'\n'
    s1 = r'\max\{'
    s2 = r'\max\{'
    s3 = r'\max\{'
    m[omega] = 0
    for k in range(0,n):
        if (w[k] <= omega):
            m[omega] = max(m[omega],m[omega-w[k]]+v[k])
            s1 += 'm['+str(omega)+'-w_'+str(k)+']+v_'+str(k)+','
            s2 += 'm['+str(omega)+'-'+str(w[k])+']+'+str(v[k])+','
            s3 += str(m[omega-w[k]])+'+'+str(v[k])+','
    s += '    m['+str(omega)+'] &= '+s1[:-1]+r'\}\\'+'\n'
    s += '                &= '+s2[:-1]+r'\}\\'+'\n'
    s += '                &= '+s3[:-1]+r'\}\\'+'\n'
    s += '                &= '+str(m[omega])+'\n'
    s += r' \end{aligned}'+'\n'+r'  \end{equation*}'+'\n'
    print (s)

print (r'\end{description}')
