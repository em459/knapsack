import numpy as np
# load the numpy library)

# weights w and values v
w = [1,2,4]
v = [2,3,9]

# bound on weight
W = 5
n = len(w)

def m(omega):
    if (omega == 0):
        return 0
    else:
        m_omega = 0
        for k in range(0,n):
            if (w[k] <= omega):
                m_omega = max(m_omega,m(omega-w[k])+v[k])
        return m_omega

print ('weights : ',w)
print ('values  : ',v)
print ('W = ',W)
print (m(W))
